\section{Design Principles}
\label{sec:design}

\begin{figure}[t]
	\centering
	\includegraphics[width=\columnwidth]{Fig02_design-requirements}
	\caption{
		(a)~An abstract model of the connection between the robot and the adhesive features, demonstrating the two design requirements.
		(b)~The solution must allow all terminal features to contact the surface when pre-loaded.
		(c)~The solution must transfer an identical load to each feature upon loading.
		}
	\label{fig:design-requirements}
\end{figure}

	An adhesive technology that is useful for real-world climbing must function under conditions of imprecise foot alignment, dynamic loading, and large surface area. For a device to obtain adhesive performance that approaches the performance of small samples tested in a stiff system, two design requirements must be met, as shown schematically in Fig.~\ref{fig:design-requirements} and described below.

\subsection{Design Requirements}

\subsubsection{Full Contact of Adhesive with Surface}

	For maximum adhesive performance, the entire adhesive should be in contact with the surface before the load is applied. If an adhesive has discrete terminal features, every feature must touch the surface.  Otherwise, only a fraction of the adhesive capability will be exploited.

\subsubsection{Even Load Distribution}

	Once the entire adhesive is in contact with the surface, the next requirement is that the \emph{load distribution} across the adhesive should be uniform.  For an adhesive with discrete features, this means that every feature experiences the same small fraction of the load. Otherwise, as the load increases, the feature with the largest portion of the load will fail prematurely, typically initiating a peeling failure that propagates across the entire patch.

\subsection{Bioinspiration for Design}

\begin{figure}[t]
	\centering
	\includegraphics[width=\columnwidth]{Fig03_gecko-foot-anatomy}
	\caption{
		Drawing of a cross-section of the gecko foot, showing inspirational functional anatomy (reprinted with permission from the Journal of Morphology~\cite{Russell_1981}).
		}
	\label{fig:gecko-foot-anatomy}
\end{figure}

	During the past decade, many research groups have studied and recreated various aspects of the gecko's adhesive apparatus, focusing on the sub-100~\micron{} level. However, when scaling to larger areas and loads, the gecko's toe provides equally useful insights at a macroscopic level.

	Each group of approximately 100~\micron{} long setae is attached to a flap called a lamella, or sometimes a scansor (Fig.~\ref{fig:gecko-foot-anatomy}).  There are around twenty lamellae per toe, each spanning the width of the toe, and arrayed from the proximal to distal end.  A lamella is only attached to the toe at the lamella's proximal end, allowing free rotation about the base~\cite{Russell_1986}.  This makes it extremely compliant in the direction normal to the surface, useful for conforming to the surface and ensuring that all the terminal features make contact, thereby satisfying the first design requirement.

	Adding to this compliance is a fluid-filled sinus or sac behind the lamellae.  The phalanges of the toe run behind this sac, so that any pre-loading pressure applied by the gecko is transferred through the sac.  The sac is instrumental in achieving full contact with the surface, even over cm-scale roughness; the fluid flows to apply a near constant pressure over all lamellae during pre-loading.

	The third feature of note is the lateral digital tendon.  This is a multiply-branched tendon that runs from the foot into each toe, where it splits once for each lamella, then again multiple times to attach across the width of the lamella at various points~\cite{Russell_1986}.  The tendon transfers the load from the skin of the gecko to the skeleton.  The tendon has three crucial attributes that help it meet the second design requirement.

	The first key attribute of the tendon is that it is highly flexible, but comparatively inextensible. It bends easily to follow surface contours, but it transmits large forces to the skeleton without stretching significantly (which would lead to stress concentrations between lamellae). Within each lamella, the tendon and other connective tissue are slightly elastic, which aids in equalizing stresses among lamellae.

	The second attribute is that the attachment point for each of the lamellae is directly at the base of the setae.  The setae lean during loading, so the tendon applies the load less than 100~\micron{} away from the surface.  The closer the point of load application is to the surface, the smaller is the pitch-back moment, and the more even is the load distribution across the adhesive (Fig.~\ref{fig:tendon-attachment}).  

	The third attribute is that the tendon essentially suspends the mass of the gecko, allowing the gecko to hang, rotating with three degrees of freedom and translating with two, while the adhesive remains affixed to the surface.  This is critical during running; if the lower limb were not decoupled in rotation from the  adhesive, a very restricted stride would be required to retain an even load distribution.

\begin{figure}[t]
	\centering
	\includegraphics[width=\columnwidth]{Fig04_single-tile-mechanism}
	\caption{
		(a)~The components of the single-tile mechanism.
		(b)~The single-tile suspending a 1~kg load from a glass slide, with significant yaw misalignment.
		(c)~1~kg load with pitch misalignment.
		(d)~1~kg load with roll misalignment (top view).
		}
	\label{fig:single-tile-mechanism}
\end{figure}

\subsection{From Requirements to Solution}

	The design requirements dictate that there must be full contact and that the load must be evenly distributed across the entire adhesive.  Unfortunately, these requirements are at least superficially conflicting: achieving full contact suggests a compliant structure, but transmitting the load suggests a stiff structure. The solution in the gecko's toe is to be highly compliant in the direction normal to the surface during pre-loading, but comparatively stiff when the tendon structure of the toe is loaded.

	With the gecko's solution in mind, two discretized synthetic mechanisms are proposed.  The first consists of a rigid tile, covered in adhesive and loaded by a tendon (Fig.~\ref{fig:single-tile-mechanism}). A soft foam support creates a compliant joint, allowing the tile to align to the surface as it is brought into contact.

	The second mechanism, for larger adhesive areas, is an array of these tiles (Figs.~\ref{fig:functional-pentadactyl}, \ref{fig:pentadactyl-schematic}). Compliant supports allow full contact of all tiles, while the pressurized sac guarantees even load distribution. In the following sections, the functional components of the mechanisms are presented, starting at the surface and moving backward toward the applied load.

\begin{figure}[t]
	\centering
	\includegraphics[width=.8\columnwidth]{Fig05_functional-pentadactyl}
	\caption{
		The functional design of the tiled array.  The compliant supports allows full contact of all tiles during pre-loading, but transmit little force during loading.  The tile tendons pass from the center of each tile to a pressurized sac, ensuring that each tile receives an identical loading force, dependent on the fluid pressure.  The resultant of these individual forces passes through the structure and into the main tendon.
		}
	\label{fig:functional-pentadactyl}
\end{figure}	

\subsubsection{Tile Backing}
\label{sec:tile-deflection}

	An ideal pre-loading and loading system would have a separate connection to every terminal feature of the adhesive. The gecko approximates this ideal with small populations of setae, each loaded by a bundle of collagen that forms the attachment point for a branch of the lateral digital tendon~\cite{Russell_1986}.  When designing for a relatively flat climbing surface, the small populations of adhesive can be increased in size, as long as they remain small in comparison to the radius of curvature of the surface. Therefore, on a surface such as glass, a macroscopic flat tile may be used.

	However, the tile may not be arbitrarily large. As tile size increases, the absolute flatness specification for achieving contact with all terminal features is constant, as the feature length does not change. However, achieving a flatness of $\pm20$~\micron{} over a 1~mm tile is much less stringent than achieving the same flatness over a 45~mm tile. Thus flatness becomes one factor limiting tile size.

	A second limiting factor is rigidity.  At the scale of the gecko's lamellae, the load is transmitted from the tendon to the setae by a bundle of collagen, nearly the size of the group of loaded setae. For the larger synthetic solution, the load from a single tendon must be distributed evenly over the large tile surface, requiring a tile with a stiff backing. 
	
	One can approximate the tile as a plate with a concentrated axial force, applied by the tendon, and a uniform distributed load, applied by the adhesive. Using the classical solution for a thin circular plate with a central force and a uniform distributed load~\cite{TimoshenkoPlateBook}, the deflection, $w$, at the center is
%
	\begin{equation}
		w = \frac{3}{16} \frac{r_0^4 P}{h^3 E} (1-\nu)(7+3 \nu),
		\label{eq:plate}
	\end{equation}
%
where $r_0$ is the radius of the plate, $P$ is the value of the uniform distributed pressure, $h$ is the thickness of the plate, and $E$ and $\nu$ are the Young's modulus and Poisson's ratio of the plate material, respectively. 

	The plate deflection solution indicates that the maximum deflection increases linearly with the plate's length scale, which again limits the maximum tile size.

\subsubsection{Compliant Support}

	The compliant support behind each tile serves several purposes.  First, it supports the tile while the foot is unattached. To this end, it is necessary that the support be stiff enough to return the unloaded foot to a stable equilibrium position whenever it detaches from the surface.  

\begin{wrapfigure}{l}{3cm}
	\includegraphics[width=3cm]{Fig06_pentadactyl-schematic}
	\caption{
		Pentadactyl foot schematic:
		(a)~Adhesive tile.
		(b)~Tile tendon.
		(c)~Compliant support.
		(d)~Pressure plate.
		(e)~Via.
		(f)~Pulleys.
		(g)~Pressur-ized sac.
		(h)~Structural plate.
		(i)~Main tendon.
		}
	\label{fig:pentadactyl-schematic}
\end{wrapfigure}

	Second, the compliant support must properly pre-load the tile. With directional adhesives, only a small normal pre-load is required to initiate adhesion~\cite{Parness_2009}. Subsequently, applying a shear load will cause the terminal features to bend, as seen in the left photograph in Fig.~\ref{fig:alignment-microwedges}a, pulling the adhesive into greater contact with the surface and increasing adhesion. It is not necessary to have a uniform pre-load pressure. However, for the self-adhering process to initiate, it is necessary for the tips of the terminal features to be in contact (e.g., Fig.~\ref{fig:alignment-microwedges}, right) over the entire area.

	In placing all features in contact with the surface, there is only a 35~\micron{} window between being fully engaged and not contacting (Fig.~\ref{fig:alignment-microwedges}a).  For an individual tile of 3~cm, this requires the angular alignment to be within~0.06{\textdegree}.  Since this alignment specification is beyond the accuracy of the climbing robots, passive alignment is required. Thus the support should be compliant enough to absorb roll and pitch errors as the foot is brought into contact with the surface.  For the tiled array, the compliant support must also allow motion perpendicular to the surface to account for discrepancies in the distances from the tiles to the wall.
	
	Third, the compliant support should not transmit forces to the adhesive due to errors in the robot's foot trajectory. In addition to the two degrees of rotational and one degree of translational movement required above, the leg should be able to rotate about an axis normal to the surface and translate laterally, so that the robot's leg does not need to maintain a constant orientation and position with respect to the adhesive during the climbing stride. 

\begin{figure}[t]
	\centering
	\includegraphics[width=.9\columnwidth]{Fig07_alignment-microwedges}
	\caption{
		(a)~With microwedges~\cite{Parness_2009}, there is a 35~\micron{} window between full engagement with the surface (left) and no engagement (right).
		(b)~This corresponds to 0.06{\textdegree} of misalignment over a 3~cm patch.
		}
	\label{fig:alignment-microwedges}
\end{figure}

\begin{figure}[t]
	\centering
	\includegraphics[width=\columnwidth]{Fig08_tendon-attachment}
	\caption{
		The pressure at the top and bottom of the tile due to different points of load application.  The center of pressure corresponds to $x=0,y=0$.
		}
	\label{fig:tendon-attachment}
\end{figure}

\subsubsection{Tendon}

	Similar to the lateral digital tendon in the gecko, the synthetic tendon has several key features.

	First, it is highly flexible, but comparatively inextensible. The bending compliance allows each tile to pivot freely, aligning to the surface during pre-loading. In the load direction, the tendons do not stretch, so that the robot does not sag away from the adhesive during climbing.

	In addition, the tendon may be attached to the tile at nearly any point.  To achieve the requirement of even load distribution, this point should be chosen to minimize pressure variation across the tile.  Assuming the point is centered side-to-side, its vertical location can be found by plotting the pressure at the top and bottom of the tile over the full range of loading angles (Fig.~\ref{fig:tendon-attachment}):
%
\begin{equation}
	P_{top,bot} = \frac{F}{wh} ( \sin\theta\pm\frac{6x\cos\theta}{h}\mp\frac{6y\sin\theta}{h} ) 
	\label{eq:ptop}
\end{equation}

	Placing the attachment point behind the center of pressure, at some distance from the wall, yields a discrepancy between the pressures at the top and bottom of the tile. Moving the attachment to the line of action of the load results in zero pitching moment and a constant pressure distribution, but only for a single loading angle.  For varying loading angles, the only point that achieves the desired result is at the wall surface, at the center of pressure.  In the presented mechanisms, the actual attachment point is within 200~\micron{} of the surface.

	A third feature of the tendon is that it does not transmit bending moments, and allows displacements in the normal and lateral directions. Once loaded, the robot is essentially suspended, decoupled from the pad in three rotational and two translational degrees of freedom (Fig.~\ref{fig:single-tile-mechanism}b--d).  This decoupling is important, because during the climbing stride, the robot leg moves with respect to the feet.  For example, with the RiSE robot, the feet rotate through almost 15{\textdegree} during climbing.

\subsubsection{Pulley}

	The tendon runs over a low-friction, non-rotating pulley (sometimes called a ``fast pulley''), transferring the tension from the vertical plane to the horizontal plane.  In the single-tile design, this is beneficial for tendon attachment because it allows the tendon to interface with the load bearing structure perpendicularly  (Fig.~\ref{fig:single-tile-mechanism}a).  In the tiled array, this change of direction of the tension allows all tendons to pass through and transmit load to the pressurized sac (Figs.~\ref{fig:functional-pentadactyl}, \ref{fig:pentadactyl-schematic}).

\subsubsection{Pressurized Sac}

	To achieve maximum adhesive performance from a tiled array, the load must be distributed evenly among the tiles just as it must be distributed evenly on each tile. Loading each tendon with a spring in a manner similar to the collagen bundles of the gecko would help to equalize forces if some tendons were slack at initial loading, but whichever spring loads first will always carry more load than the others.  For near-uniform load distribution, a closer to ideal solution is to use a pressurized sac (which serves a different purpose from the pre-loading sinus on the gecko). The fluid pressure increases rapidly with increasing load, and is applied equally to the pressure plates attached to each of the tiles.