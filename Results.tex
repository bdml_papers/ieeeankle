\section{Results}
\label{sec:results}

	The bioinspired single-tile mechanism is demonstrated on two climbing robots, StickybotIII (0.8~kg) and the RiSE robot~\cite{Spenko_2008} (4~kg with payload), which is over an order of magnitude heavier than any previous climbing robot using gecko inspired adhesives.  Further, the tiled array of 100~cm$^{2}$ is shown to be capable of supporting 34~kg at 15\textdegree{}. 

\subsection{Single-Tile Mechanisms on Robotic Platforms}

	To evaluate a solution for climbing, it is informative to plot the contact forces exerted by the robot and to compare them with the limits of the adhesive mounted on the single-tile mechanism, expressed as a limit curve in a two-dimensional space of normal and shear forces per unit area.

\subsubsection{Methods}

	Force data are collected from both robots during vertical climbing.  For StickybotIII, an acrylic wall with a 6~cm square cutout is fabricated.  Inside the cutout, a 6-axis force-torque sensor (ATI Industrial Automation Gamma \mbox{SI-32-2.5}, accuracy $\pm$0.05~N) is mounted with a smooth acrylic plate fastened to its surface, coplanar with the wall.  For the RiSE robot, normal and shear forces are logged with onboard strain gauges built into the legs, each accurate to 0.25~N. In both cases, a single gait cycle of a single limb is recorded.

	To obtain the adhesion limits for the single tile mechanism, the same force sensor is used, with a glass slide affixed to its surface.  A forelimb of StickybotIII with 7~cm$^{2}$ of adhesive is intentionally misaligned and brought into contact with the glass.  Once full contact with the surface is made, a load is applied to the forelimb until failure.  The test is repeated through a range of pull-off angles to populate the limit curve. 

	To get an ideal ``benchtop'' limit curve for a small test sample of the adhesive, an approximately 0.7~cm$^{2}$ square of adhesive is attached to the surface of the force sensor.  A three-axis stage ($\pm$10~\micron{} accuracy in the tangential direction, $\pm$1~\micron{} in the normal direction) then brings a glass slide into contact with the sample.  The sample is aligned to the glass using a microscope and a manual two-axis goniometer. The stage then follows a series of preset loading and pull-off trajectories, and the forces are recorded at failure. 

\begin{figure}[t]
	\centering
	\includegraphics[width=.9\columnwidth]{Fig11_robot-gait-stresses}
	\caption{
		Top: The stress upon a single foot during one gait cycle of StickybotIII.
		Bottom: The stress upon a single foot of the RiSE platform. Dotted lines show maximum loading angles.}
	\label{fig:robot-gait-stresses}
\end{figure}

\subsubsection{Force Data}

	The climbing data from StickybotIII and the RiSE platform are shown in Fig.~\ref{fig:robot-gait-stresses} in a two-dimensional space of normal and shear stress. Both robots show a basic pattern of (a)~pre-loading with a positive normal pressure, (b)~loading in shear and attaining adhesion, then (c)~removing shear and (d)~returning to the origin of force space.  This final step permits efficient climbing, as there is a nearly zero adhesive force once the shear is removed. For video footage demonstrating the gait, see website \cite{BDMLvideo}.
	
	Because directional adhesives can sustain higher stresses in shear than in normal, the point of maximal normal stress is the most likely point of catastrophic failure. This point is specified by an angle in force space and a distance from the origin.  The angle is determined by the gait and structure of the robot.  The distance from the origin, or magnitude of the total force per unit area, is determined by the area of the adhesive relative to the mass of the robot.  For StickybotIII, the angle is 9\textdegree{} and the force per unit area has a magnitude of 6.1~kPa.  The RiSE robot was not designed for climbing glass, and has a center of mass well away from the wall, resulting in an angle of 15\textdegree{}.  It is also scaled with respect to StickybotIII, and therefore has a larger mass to surface area ratio, as seen in the 7.5~kPa force magnitude per unit area.  

\begin{figure}[t]
	\centering
	\includegraphics[width=\columnwidth]{Fig12_single-tile-vs-benchtop}
	\caption{
		The limit curve of a 0.7~cm$^{2}$, well-aligned patch tested in a benchtop setup (black dots) compared to that of the 7~cm$^{2}$ single-tile mechanism (blue crosses).  StickybotIII and RiSE gait data are plotted in the upper left, well inside the shaded safe region.  The loading angle of StickybotIII and geometrically scaled versions of the same robot are shown by a dashed line.  If scaled 7 times, the robot retains a sufficient factor of safety (F.S.\ on plot), even as the required pressures move beyond 40~kPa.
		}
	\label{fig:single-tile-vs-benchtop}
\end{figure}
  
\subsubsection{Limit Curve}

	With the force data in hand, it remains to look at the limit curve of the single tile mechanism.  At minimum, the limit curve needs to enclose all points from the force data for safe climbing; it must have a both a greater maximum angle and a greater maximum stress.  
	
	In the ideal case, if the single-tile mechanism allowed perfect scaling of the adhesive, achieving perfect passive alignment for 100\% contact and perfect load distribution across the large area, the limit curve would match data obtained with a small sample in benchtop testing.  Approaching the benchtop limit curve has yet another challenge.  The benchtop data are obtained from a stiff system making controlled displacements and measuring forces; localized peeling is often not catastrophic in this case. In contrast, with the controlled force tests used to test the single-tile (and seen in robot climbing), localized failures are usually catastrophic. Thus any non-ideality in contact or loading will cause immediate failure.
	
	The empirical limit curve for benchtop testing is shown in Fig.~\ref{fig:single-tile-vs-benchtop} as a series of large black dots, and is well beyond the requirements of either StickybotIII or the RiSE robot, shown as small dots near the origin (from Fig.~\ref{fig:robot-gait-stresses}). The empirical limit curve for the tendon-based mechanism is shown as a series of blue crosses.  The single-tile mechanism obtains a level of performance comparable to the benchtop data.
	
	To compare the current results with the original foot of StickybotIII \cite{Hawkes_2011}, a metric called the \emph{efficacy} is introduced. Suppose an adhesive is installed in a certain mechanism and loaded at a given angle, $\theta$, at which it supports a maximum adhesive force magnitude per unit area of $f_{m}$. Next, suppose the adhesive also supports a maximum force per unit area of $f_{b}$ in a bench-top test at the same angle. Then let the efficacy of the adhesive mechanism at $\theta$ be defined as $\epsilon = f_{m} / f_{b}$.
	
	StickybotIII has a maximum loading angle of $\theta$~= 10\textdegree{} during climbing.  At this angle, the original StickybotIII foot mechanism with directional adhesives can support an adhesive force magnitude per unit area of $f_m$~= 8~kPa, while the same adhesives attain $f_b$~= 58~kPa in a bench-top test. Thus the efficacy for the original foot mechanism is $\epsilon$~= 0.14. For the single-tile mechanism at $\theta$~= 10\textdegree{}, the maximum force magnitude per unit area is also $f_m$~= 58~kPa, so the single-tile mechanism therefore has an efficacy of $\epsilon$~= 1.00. StickybotIII can climb well with both mechanisms, but the large increase in efficacy with the new single tile mechanism greatly increases the safety factor for StickybotIII and allows the robot to be scaled to a much larger size.

\subsubsection{Scaling}

	Using a mechanism with such a high efficacy, a robot much larger than StickybotIII or the RiSE robot could theoretically be made to climb using the same adhesive materials. In Fig.~\ref{fig:single-tile-vs-benchtop}, a dashed purple line shows the loading angle at maximum force per unit area (10\textdegree{}) for climbing robots with approximately the same proportions as StickybotIII.  Following this line suggests that StickybotIII could be scaled 7 times larger to 275~kg, increasing the required force per unit area by 7 times while retaining a factor of safety of 1.5 (F.S.\ in Fig.~\ref{fig:single-tile-vs-benchtop}). This factor of safety is sufficient for reliable climbing when using directional adhesives, as demonstrated in previous work~\cite{Hawkes_2011}. However, at this scale, it becomes difficult to achieve adequate flatness and stiffness with a single tile, motivating the use of a tiled array as described in the next section.

\subsection{Pentadactyl Hand}

	As the robot is scaled to sizes larger than the RiSE platform, it becomes increasingly desirable to use arrays of tiles to increase robustness in case of contamination of a small area of the adhesive, to adapt to surfaces that are not perfectly flat, and to achieve sufficient tile stiffness during loading.

\begin{figure}[t]
	\centering
	\includegraphics[width=.9\columnwidth]{Fig13_pentadactyl-test}
	\caption{
		Left: Pentadactyl hand supporting 22.5~kg. Right: Test apparatus with pentadactyl hand holding 17.0~kg at 22.4\textdegree{}.
		}
	\label{fig:pentadactyl-test}
\end{figure}

\subsubsection{Methods}

	For the five tile array, the forces are too high for the multi-axis force sensor used in previous tests. Instead, a simple method is used in which the tiles are attached to a small plate of vertical glass and loaded by a mass hanging from the main tendon.  The glass is then slowly tilted away from vertical until the sample fails (the tilting is done slowly enough to avoid significant dynamic forces).  The whole procedure is captured on video at 30~fps.  The video frame before the failure is then analyzed, and the angle of the glass is measured with respect to a vertical reference (Fig.~\ref{fig:pentadactyl-test}, right).

\subsubsection{Limit Curve}

	The data from a single tile (20~cm$^{2}$) of the array are shown as blue diamonds in Fig.~\ref{fig:pentadactyl-vs-single} and data for the entire array (5 times larger, at 100~cm$^{2}$) are shown as red squares. In both cases, the normal and shear stresses are slightly lower than in previous tests because a different adhesive sample, with slightly poorer performance, was used. Although it is considerably larger, the array performs very similarly to the single tile, with nearly the same ratio of normal stress to shear stress, corresponding to a sector between 15\textdegree{} and 20\textdegree{} in force space.
	
	With these results, a 60~kg robot with proportions similar to StickybotIII should be able to climb using a diagonal gait and a pentadactyl array on each foot.  Further, since there is no noticeable drop-off of performance scaling from a single tile to five tiles with five times the adhesive area, it may be possible to array a dozen or more tiles, allowing humans to climb glass.
