\section{Fabrication}

\subsection{Adhesive}
\label{sec:adhesive-fab}

	The adhesive technology used in the climbing mechanism is based on earlier work on arrays of wedge-shaped silicone features with a 20~\micron{} base width and a height of approximately 80~\micron{}~\cite{AutumnAdhesion2006, Parness_2009, SotoAppPhys2010}. Like the adhesives of the gecko, these features exhibit ``directional adhesion.'' In the unloaded state, the material is not sticky, as only the tips of the wedges are exposed, resulting in a very low contact area. When brought into contact and loaded in shear, the wedges bend over (Fig.~\ref{fig:alignment-microwedges}a, left), greatly increasing the contact area. The amount of adhesion can thus be controlled by varying the applied shear load. To detach the adhesive, it suffices to remove the shear load, causing the wedges to straighten and detach. Thus attachment and detachment can be accomplished with little wasted energy (unlike the case with sticky, pressure sensitive adhesives).

	Previously, a photolithographic fabrication process was used to produce these adhesive structures \cite{Parness_2009}, but that process was confined to a 102~mm diameter wafer and required two precisely-aligned and angled exposures.  Motivated by the need to produce larger patches of material, a new mold-making process has been developed.

\subsubsection{Mold Preparation and Casting}

	In the new process, a wax mold is prepared by micromachining with a sharp tool, specifically a polytetrafluoroethylene (PTFE) coated disposable steel microtome knife. The tool and the mold are fixed in a computer numerical control (CNC) milling machine.  A liquid soap lubricant prevents prevents material from sticking to the tool. With adequate alignment, the resulting adhesive patch area is limited only by the size of the CNC machine and the width of the tool. With the current apparatus, an uninterrupted rectangular patch as large as $762 \times 76$ mm is possible.

	The tool is driven into the mold along a trajectory that is nearly parallel to the tool's rear face. This process is a hybrid of classical orthogonal machining~\cite{Merchant1945a} and oblique indenting~\cite{Hill1946, Meguid1977}, and it is chosen so that the zone of plastic deformation is predominantly ahead of the tool, minimizing deformation of completed cavities behind the tool.  This trajectory allows a densely packed array of sharp, angled cavities. For the samples used in the reported tests, the cavities are nominally 102~\micron{} deep (measured normal to the surface) and spaced 61~\micron{} apart tip-to-tip, with an included angle of 24\textdegree{} (Fig.~\ref{fig:micromachining-process}, right).

	To create features, a degassed polydimethylsiloxane (PDMS) silicone elastomer (Dow Corning Sylgard~170) is cast into the mold. The PDMS is allowed to cure for 24 hours at room temperature (Fig.~\ref{fig:micromachining-process}, left).

\begin{figure}[t]
	\centering
	\includegraphics[width=\columnwidth]{Fig09_micromachining-process}
	\caption{
		Left: scanning electron micrograph of the completed microwedges after removal from the wax mold.
		Right: diagram of the micromachining process.
		}
	\label{fig:micromachining-process}
\end{figure}

\subsection{Single Tile Fabrication}
	\label{sec:single-tile-fab}

	The 25~mm square tiles are cut from 1.5~mm thick glass fiber composite using a CO$_2$ laser cutter. The surface is sanded and coated with a primer (Dow Corning PR-1200).  The PDMS wedge layer is bonded directly to the tile by setting the glass fiber sheet into the uncured PDMS, suspending it 300~\micron{} above the surface of the wax mold.

	A 3~mm square of silicone foam is secured to the back center of the tile with a room temperature vulcanizing (RTV) silicone adhesive and to the glass fiber forelimb extension of the robot. The tendon is made from 13~kg-test gel-spun polyethylene braided cord tied around a 0.75~mm steel rod next to the foam and threaded through a hole to the surface of the tile just above the center of pressure.  It passes along a groove down to the center of pressure, then into a wedge-shaped cutout directly below the center of pressure (Fig.~\ref{fig:single-tile-mechanism}a). The normal tension from the tendon is therefore transferred as directly as possible to the center of pressure.  The tendon then passes over the fixed pulley and through the forelimb extension, behind which it is secured to another steel rod.
       
\subsection{Pentadactyl Hand}

	Each tile of the array (Fig.~\ref{fig:pentadactyl-photo}) is laser-cut into a 44~mm square following the process for a single tile. 

\begin{figure}[t]
	\centering
	\includegraphics[width=8cm]{Fig10_pentadactyl-photo}
	\caption{
		Side and ventral view of pentadactyl array, consisting of five 20~cm$^2$ tiles.
		}
	\label{fig:pentadactyl-photo}
\end{figure}	

	The large structural plate is laser-cut from 6~mm thick aircraft plywood, with a center wedge cutout for the loading tendon and through-holes for the tendons.  The pulleys are laser-cut from 3~mm acrylonitrile butadiene styrene (ABS) and affixed to the structural plate.  A 6~mm square of silicone foam is bonded to the ABS and tile backing.  Each tile is also linked to its neighbors with an extra-soft foam, preventing large rotation while not adhered (Fig.~\ref{fig:pentadactyl-photo}).

	A double-strand tendon is attached to each tile in the same manner as for the single tile.  Each tendon then runs over its fixed pulley and through the structural plate.  It passes through a via in the pressurized sac and through a 0.75~mm thick 44~mm square pressure plate on the back of the sac.  It is finally tied to a steel rod on the back of the plate.  The pressurized sac is manufactured from two layers of 150~\micron{} thick polyethelene film which are impulse heat sealed around the edges.  The vias are created first by sealing the two layers with a point impulse, then by punching a hole through the sealed region.  The sac is filled with water, and the entrance port is sealed.  The main load bearing tendon is eight strands of braided cord which pass from the load, over the central pulley, through the structural plate and a steel washer, and loop around a steel rod.
