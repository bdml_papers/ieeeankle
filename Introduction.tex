\section{Introduction}
\label{sec:intro}

	\IEEEPARstart{T}{he} gecko is famous for its ability to climb many surfaces rapidly and efficiently. Considerable research has been devoted to understanding how the gecko's setae and spatulae contribute to this ability, by conforming to surfaces, generating adhesion using van der Waals forces, and exploiting directional features for low-effort attachment and detachment \cite{Autumn2000, Autumn_2002, AutumnAdhesion2006, Gravish2010Rate}. 

	Drawing inspiration from the gecko, numerous synthetic dry adhesives have also been developed, many of which exhibit impressive adhesion~\cite{DelCampo_2007, Gorb_2007, Lee_2008, Lee_2008b, Qu_2008, Davies_2009, Maeno_2009, Murphy_2009, Sameoto_2009, Jeong_2009, Sameoto_2010, Gillies_2011}. However, most of this work has focused on benchtop testing with carefully aligned materials and substrates, and small ($< 1$~cm$^2$) samples of material.  With a few exceptions (e.g., \cite{Hawkes_2011,BartlettCrosby2012}), much less work has focused on scaling materials to large areas and loads while maintaining useful levels of adhesion, and on applying them to robotic climbing, where alignment and load application are imprecise.

\begin{figure}[ht]
	\centering
	\includegraphics[width=.8\columnwidth]{Fig01_rise-climbing-granite}
	\caption{
		Four-legged version of the RiSE robot \cite{Spenko_2008} (4~kg with payload) climbing with a diagonal stride gait on vertical granite (average roughness 850~nm~\cite{Grissom2000}), using 20~cm$^2$ tiles. Inset: underside of foot at point in the stride requiring almost 10\textdegree{} of alignment compensation (tendon visible at center).
		}
	\label{fig:rise-climbing-granite}
\end{figure}

	Among the robots that climb using either flat or micro-structured dry adhesive materials, none has achieved working levels of adhesion comparable to those obtained in benchtop tests~\cite{Murphy2007, Kim_2008, Daltorio_2009, Unver_2010, Murphy_2010, Krahn_2011, Seo2012}. For example, the original 0.3~kg Stickybot climbing robot~\cite{Kim_2008} used ``directional polymeric stalks'' and climbed with a small factor of safety, applying 16~cm$^{2}$ of adhesive to the wall at each step. Extrapolating from benchtop tests with the same material, this amount of adhesive should have been able to support a 5~kg robot. In other work, Murphy \emph{et al.}~\cite{Murphy_2010} noted that carrying a payload much over 0.5~kg was impractical for a Waalbot robot design of any size, even though benchtop tests suggest much higher loads are possible~\cite{Kim_2007}. 

\IEEEpubidadjcol

	Small robots such as Stickybot and Waalbot can compensate for this performance reduction by using larger feet, but it becomes difficult to make feet large enough to supply the required adhesion as the robot's scale, $l$, increases further (mass increases as $l^3$ while contact area increases as $l^2$). For a large robot to climb and carry payloads (Fig.~\ref{fig:rise-climbing-granite}), it must achieve levels of adhesion comparable to those obtained in benchtop tests, even as the contact area at each foot grows to hundreds of square centimeters. Moreover, the solution must be tolerant of dynamic load application and significant variations in alignment. To address this problem, it is again useful to draw inspiration from the gecko, this time looking at the macroscopic structures of the gecko's toe, which conform to surfaces and ensure relatively uniform pressures as loads are applied.

	The following section presents design principles that make it possible to scale dry adhesives to large areas and loads for climbing applications. Features of the gecko's toe that provide these capabilities are briefly reviewed and two discretized mechanical designs having similar capabilities are proposed.

	First, a mechanism making use of an adhesive tile, a tendon-like string, and a compliant support is tested and compared to a small benchtop test sample and is demonstrated on two different climbing platforms: StickybotIII (0.8~kg) and a quadrupedal version of the RiSE platform (4~kg with payload).  In addition, an expanded second mechanism which combines multiple independent tiles into an array is tested and compared to the single-tile mechanism. This tiled array mechanism uses 100~cm$^{2}$ of adhesive and can support 34~kg at a 15{\textdegree} loading angle when placed inexactly by a human operator.

	The results suggest that the mechanisms achieve constant adhesive performance no matter how large they are scaled. In addition, the need to work with large patches of material has led to a new manufacturing process, described in Sec.~\ref{sec:adhesive-fab}, that makes it possible to produce hundreds of square centimeters of material at a time.
